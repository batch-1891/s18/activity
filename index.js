/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function




	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/
// 1.
function sumOfTwoNumbers(firstNumber, secondNumber){
	console.log('Displayed sum of 5 and 15')
	let sum = 0
	sum = firstNumber + secondNumber
	console.log(sum)
}
sumOfTwoNumbers(5, 15)

function subtOfTwoNumbers(firstNumber, secondNumber){
	console.log('The product of 50 and 10:')
	let sum = 0
	sum = firstNumber - secondNumber
	console.log(sum)
}
subtOfTwoNumbers(20, 5)


// 2.
function multiOfTwoNumbers(firstNumber, secondNumber){
	console.log('The product of 50 and 10:')
	let multi = 0
	multi = firstNumber * secondNumber
	return multi
}
console.log(multiOfTwoNumbers(50, 10))

function quotientOfTwoNumbers(firstNumber, secondNumber){
	console.log('The product of 50 and 10:')
	let quotient = 0
	quotient = firstNumber / secondNumber
	return quotient
}
console.log(quotientOfTwoNumbers(50, 10))


// 3.
function radiusArea(radius){
	console.log('The result of getting the area of a circle with 15 radius:')
	let totalArea = 0
	totalArea = radius * radius * 3.14
	return totalArea
}
console.log(radiusArea(15))


// 4.
function quotientOfFourNumbers(firstNumber, secondNumber, thirdNumber, fourthNumber){
	console.log('The average of 20,40,60 and 80:')
	let averageOfFourNumber = 0
	averageOfFourNumber = (firstNumber + secondNumber + thirdNumber + fourthNumber) / 4
	return averageOfFourNumber
}
console.log(quotientOfFourNumbers(20, 40, 60, 80))


// 5.

let isPassingscore = averageOfTwoNumbers > 75
function averageOfTwoNumbers(firstNumber, secondNumber){
	console.log('Is 38/50 a passing score?')
	let isPassed = 0
	isPassed = firstNumber / secondNumber * 100
	let isPassingScore = isPassed > 75
	console.log(isPassingScore)
	return isPassed
}
averageOfTwoNumbers(38, 50)

